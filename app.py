from flask import Flask, send_from_directory, render_template,jsonify
from flask.ext.mongoengine import MongoEngine, DoesNotExist

app = Flask(__name__)
app.secret = 'Cool story from some random places'
app.config['UPLOAD_FOLDER'] = 'static'
db = MongoEngine()
app.config['MONGODB_DB'] = 'TWEETSEG'
app.config['MONGODB_HOST'] = 'localhost'
app.config['MONGODB_PORT'] = 27017
app.config['MONGODB_USERNAME'] = ''
app.config['MONGODB_PASSWORD'] = ''

db.init_app(app)

class TweetSegments(db.Document):
	text = db.StringField(required=True)
	bigram = db.ListField()
	trigram = db.ListField()
	quadgram = db.ListField()
	pentagram = db.ListField()



@app.route("/")
def hello():
    return render_template('index.html')

@app.route("/data")
def Data():
	return render_template('data.html')
@app.route('/<path:filename>')
def download_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename, as_attachment=True)

@app.route("/getTweetData")
def getTweetData():
	try:
		ts = TweetSegments.objects()
		data = [{'text': t.text,'bigram':t.bigram,'trigram':t.trigram,'quadgram':t.quadgram,'pentagram':t.pentagram} for t in ts]
		return jsonify({'status': True,'data':data})
	except Exception as e:
		print e
	return jsonify({'status': False,'data':None})


if __name__ == "__main__":
    app.run(debug=True, port=2345)