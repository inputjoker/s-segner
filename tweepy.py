import string
from mongoengine import *
from twython import Twython
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from nltk.util import ngrams
import re
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import wordpunct_tokenize
from nltk.stem.wordnet import WordNetLemmatizer

connect('TWEETSEG')

class Tweet(Document):
	text = StringField(required=True)
	username = StringField()

class TweetSegments(Document):
	text = StringField(required=True)
	bigram = ListField()
	trigram = ListField()
	quadgram = ListField()
	pentagram = ListField()

	
### EXTRACTING TWEET FROM TWITTER ###
def main():
	APP_KEY = 'DfcHcsSuzi8SD53MpoXWjRNz3'
	APP_SECRET = 'zo8j200fKkq5RaltDJMFWKF9bepcuwlJeTneDL8twr5u7BXo1j'
	ACCESS_TOKEN = ''
	ACCESS_TOKEN_SECRET = ''
	twitter = Twython(APP_KEY, APP_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
	search_results = twitter.search(q="republic day")
	data = search_results
	for status in data['statuses']:
		Tweet(text = status['text']).save()

### TWEET SEGMENTATION ###
def tokenit():
	stop_words = set(stopwords.words('english'))
	stop_words.update(['.', ',', '"', "'", '?', '!', ':', ';', '(', ')', '[', ']', '{', '}','#','rt','@','/','//','://'])
	lmtzr = WordNetLemmatizer()
	tweets_obj = Tweet.objects()
	tweets = [tweet.text for tweet in  tweets_obj]
	for t in tweets:
		t_data =  t.encode("ascii","ignore")
		words = [lmtzr.lemmatize(i.lower(),'v') for i in wordpunct_tokenize(t_data) if i.lower() not in stop_words]
		bigram = [tuplelist for tuplelist in ngrams(words,2)]
		trigram = [tuplelist for tuplelist in ngrams(words,3)]
		quadgram = [tuplelist for tuplelist in ngrams(words,4)]
		pentagram = [tuplelist for tuplelist in ngrams(words,5)]
		ts = TweetSegments(text=t,bigram=bigram,trigram=trigram,quadgram=quadgram,pentagram=pentagram)
		ts.save()
		print "saved"
		# fdist = nltk.FreqDist(words)
		# for word, frequency in fdist.most_common(50):
		# 	print('%s;%d' % (word, frequency)).encode('utf-8')

		
if __name__ == '__main__':
	i = input("1. Main(collect tweets)\n2.Tokenize\n")
	if i == 1:
		main()
	if i == 2:
		tokenit()

	